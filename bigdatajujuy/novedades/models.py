from __future__ import unicode_literals
import datetime
from django.db import models
from django.contrib.auth.models import *
from django.utils import timezone

#Import Extras
from tinymce.models import HTMLField
from taggit.managers import TaggableManager

#Choice definitions

#Create your models here.
class Noticia(models.Model):
    titulo = models.CharField('Titulo', max_length=200)
    portada = models.FileField(upload_to='novedades/', default='/archivos/defaults/noimage.gif')
    epigrafe = models.CharField('Epigrafe', default='imagen' ,max_length=50)
    etiquetas = TaggableManager()
    cuerpo = HTMLField()
    fecha = models.DateTimeField('Fecha', default=datetime.datetime.now)
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    destacada = models.BooleanField(default=False)
    def __str__(self):
        return self.titulo