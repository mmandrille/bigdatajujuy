from django.apps import AppConfig


class NovedadesConfig(AppConfig):
    name = 'novedades'
    verbose_name = "<<Novedades>>"
    