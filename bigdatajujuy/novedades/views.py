from django.shortcuts import render
from django.http import HttpResponse
#Import Personales
from .models import Noticia

# Create your views here.
def novedades(request):
    noticias = Noticia.objects.all().order_by('fecha')[:5]
    return render(request, 'novedades.html', { 'noticias': noticias, }) 

def noticia(request, id_noticia):
    noticia = Noticia.objects.get(pk=id_noticia)
    return render(request, 'noticia.html', { 'noticia': noticia, })

def carousel(request):
    noticias = Noticia.objects.filter(destacada=True).order_by('fecha')[:5]
    return render(request, 'carousel.html', {'noticias': noticias,})