from django.conf.urls import url
from django.urls import path
#Import de modulos personales
from . import views

app_name = 'novedades'
urlpatterns = [
    path('', views.novedades, name='novedades'),
    path('<int:id_noticia>', views.noticia, name='noticia'),
    path('carousel', views.carousel, name='carousel'),
]