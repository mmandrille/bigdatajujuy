from django.contrib import admin
from .models import Evento
# Register your models here.
class EventoAdmin(admin.ModelAdmin):
    ordering = ['fecha_inicio']
    list_filter = ['importante', 'tipo', ]

#Los mandamos al admin
admin.site.register(Evento, EventoAdmin)
